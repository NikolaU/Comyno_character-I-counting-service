package count;

public class Counter {

    public int countI(String sequence) {

        int counter = 0;

        if (sequence != null && !sequence.equals("")) {
            for (int i=0; i < sequence.length(); i++) {
                if (sequence.charAt(i) == 'i') {
                    counter++;
                }
            }
            return counter;
        } else {
            return 0;
        }

    }

}

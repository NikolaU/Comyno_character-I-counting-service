<%-- 
    Document   : index
    Created on : Feb 25, 2018, 8:33:30 PM
    Author     : Nikola
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Character Counter</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href='https://fonts.googleapis.com/css?family=Cherry Swash' rel='stylesheet'>
        <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    </head>
    <body>

        <div id='logo'>
            <p>InfinityEyes D.O.O.</p>
        </div>

        <h1 id="manyi">How many I's?</h1>

    <center>
        <form id="icounter" action="CounterServlet" method="POST" autocomplete="off" >
            Enter your sequence: 
            <input type="text" name="sequence" id="sequence" required="true"  >
            <button type="submit" >Count</button>
        </form>

        <p>Result: <span id="result">${sum}</span></p>
    </center>

    <script>
        $(document).ready(function () {                                                   
            $('#icounter').submit(function () {                                         
                $form = $(this);                                                         
                $.post($form.attr('action'), $form.serialize(), function (responseText) { 
                    $('#result').text(responseText);                                     
                });
                return false;                                                            
            });
        });
    </script>

</body>
</html>

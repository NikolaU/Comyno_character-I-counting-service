<%-- 
    Document   : error404
    Created on : Feb 26, 2018, 3:08:57 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href='https://fonts.googleapis.com/css?family=Cherry Swash' rel='stylesheet'>
        <title>404 Error</title>
    </head>
    <body>

        <div id='logo'>
            <p>InfinityEyes D.O.O.</p>
        </div>

    <center>
        <h1>Required resource is not found!</h1>
        <a href="index.jsp">Go back to previous page.</a>
    </center>
</body>
</html>
